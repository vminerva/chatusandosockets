package br.com.sockets.control;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionChat extends Observable {
	
	private String ip;
	private String nome;
	private String message;
	
	public ConnectionChat(String ip, String nome) {
		this.ip = ip;
		this.nome = nome;
		new Thread(new Recebe()).start();
	}
	public void envia(String texto) {
        new Thread(new Envia(texto)).start();
    }

    public void notifica(String msg) {
        this.message = msg;
        setChanged();
        notifyObservers();
    }

    class Recebe implements Runnable {

        byte[] dadosReceber = new byte[255];
        boolean erro = false;
        DatagramSocket socket = null;

        @Override
        public void run() {
            while (true) {
                try {
                    socket = new DatagramSocket(5000);
                } catch (SocketException ex) {
                    Logger.getLogger(ConnectionChat.class.getName()).log(Level.SEVERE, null, ex);
                }
                erro = false;
                while (!erro) {
                    DatagramPacket pacoteRecebido = new DatagramPacket(dadosReceber, dadosReceber.length);
                    try {
                        socket.receive(pacoteRecebido);
                        byte[] entrada = pacoteRecebido.getData();
                        String texto = "";
                        for (int i = 0; i < entrada.length; i++) {
                            if (entrada[i] != 0) {
                                texto += (char) entrada[i];
                            }
                        }
                        String nome = getNome() + " disse: ";
                        notifica(nome + texto);
                    } catch (Exception e) {
                        System.out.println("erro");
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(ConnectionChat.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        erro = true;
                        continue;
                    }
                }
            }
        }
    }

    class Envia implements Runnable {

        String texto;

        public Envia(String texto) {
            this.texto = texto;
        }

        @Override
        public void run() {

            byte[] dados = texto.getBytes();

            try {
                DatagramSocket clientSocket = new DatagramSocket();
                InetAddress address = InetAddress.getByName(getIp());
                DatagramPacket pacote = new DatagramPacket(dados, dados.length, address, 5000);
                clientSocket.send(pacote);
                clientSocket.close();
                
                
            } catch (SocketException ex) {
                Logger.getLogger(ConnectionChat.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnknownHostException ex) {
                Logger.getLogger(ConnectionChat.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ConnectionChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

	public String getIp() {
		return ip;
	}

	public String getNome() {
		return nome;
	}

	public String getMessage() {
		return message;
	}
}
