package br.com.sockets.control;

import javax.swing.JOptionPane;

import br.com.sockets.view.JanelaChat;

public class ProgramaChat {
	public static void main(String[] args) {
		String ip = (String) JOptionPane.showInputDialog(null, "Informe o IP:",
				"Chat @by vminerva", JOptionPane.QUESTION_MESSAGE, null, null,
				"192.168.0.0");
		String nome = (String) JOptionPane.showInputDialog(null,
				"Informe seu NOME e SOBRENOME:", "Chat @by vminerva",
				JOptionPane.QUESTION_MESSAGE, null, null, "Seu nome e sobrenome");
		if (ip != null && nome != null) {
			ConnectionChat connectionChat = new ConnectionChat(ip, nome);
			JanelaChat frame = new JanelaChat(connectionChat);
			frame.setVisible(true);
		}else{
			JOptionPane.showMessageDialog(null,
					"Voc� precisa preencher os campos!",
					"ERRO - Campos Obrigat�rios", JOptionPane.WARNING_MESSAGE,
					null);
		}
	}
}
