package br.com.sockets.view;

import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import br.com.sockets.control.ConnectionChat;

public class JanelaChat extends JFrame implements Observer{

	private static final long serialVersionUID = -2435976143071071144L;

	private ConnectionChat connection;
	private JPanel panelShowMessages = new JPanel();
	private JPanel panelTextAreaDigit = new JPanel();
	private JTextArea textAreaMessage = new JTextArea();
	private JTextArea textAreaChat = new JTextArea();
	private JButton btnEnviar = new JButton("Enviar");
	
	public JanelaChat(ConnectionChat connection){
		super("Chat - by @vminerva");
		this.connection = connection;
		init();
		connection.addObserver(this);
		writeInPanelShowMessage("Chat iniciado com "+connection.getIp()+" | Nome: "+connection.getNome());
		textAreaMessage.requestFocusInWindow();
	}

	/**
	 * Create the frame.
	 */
	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 490, 493);
		getContentPane().setLayout(null);
		
		panelShowMessages.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelShowMessages.setBounds(10, 11, 454, 275);
		getContentPane().add(panelShowMessages);
		panelShowMessages.setLayout(null);
		textAreaChat.setEditable(false);
		textAreaChat.setRows(5);
		textAreaChat.setColumns(20);
		
		textAreaChat.setBackground(SystemColor.menu);
		textAreaChat.setBounds(1, 1, 452, 273);
		panelShowMessages.add(textAreaChat);
		
		panelTextAreaDigit.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelTextAreaDigit.setBounds(10, 297, 454, 111);
		getContentPane().add(panelTextAreaDigit);
		panelTextAreaDigit.setLayout(null);
		
		textAreaMessage.setBounds(1, 2, 452, 107);
		textAreaMessage.addKeyListener(new KeyAdapter() {
	            public void keyReleased(java.awt.event.KeyEvent evt) {
	            	clickEnterButtonSendMessage(evt);
	            }
	        });
		panelTextAreaDigit.add(textAreaMessage);
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textAreaMessage.getText().isEmpty()) {
		            sendMessage();
		        }
			}
		});
		btnEnviar.setBounds(375, 419, 89, 23);
		getContentPane().add(btnEnviar);
	}
	
	private void sendMessage() {
		if (!textAreaMessage.getText().isEmpty()) {
			connection.envia(textAreaMessage.getText());
			writeInPanelShowMessage("Voc� disse: " + textAreaMessage.getText());
			textAreaMessage.setText("");
		}
	}

	public void writeInPanelShowMessage(String text){
		textAreaChat.append(text+"\n");
		if (!textAreaChat.getText().isEmpty() && !textAreaChat.isFocusOwner()) {
			textAreaChat.setCaretPosition(textAreaChat.getText().length() - 1);
		}
		
	}
	
	public void clickEnterButtonSendMessage(KeyEvent e){
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			sendMessage();
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		writeInPanelShowMessage(connection.getMessage());
	}
}
